import React, { Component } from 'react';
import './Cr.scss';

export default class Cr extends Component {
  render() {
    const { modifer, withHover, onClick } = this.props;
    return (
      <div className={`cr ${withHover && 'cr_hovered'} ${modifer || ''}`}></div>
    );
  }
}
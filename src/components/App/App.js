import { Fragment } from 'react';
import Header from '../Hrader/Header';
import Footer from '../Footer/Footer';
import Cr from '../Cr/Cr';
import './App.scss';

export default function App() {
  return (
    <Fragment>
      <Header />
      <main>
        <div>
          <Cr modifer= 'cr_grey'/>
          <Cr modifer= 'cr_yellow' withHover/>
        </div>
        <div>
          <Cr modifer= 'cr_yellow' withHover/>
          <Cr />
        </div>
        <div>
          <Cr modifer= 'cr_black'/>
          <Cr />
        </div>
        <div>
          <Cr modifer= 'cr_yellow' withHover/>
          <Cr modifer= 'cr_yellow' withHover/>
        </div>
      </main>
      <Footer />
    </Fragment>
  );
}
